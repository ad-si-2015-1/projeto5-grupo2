/*jslint node:true*/
/*global describe, it */

var should = require("should"),
    assert = require("chai").assert,
    array = require(process.cwd()).framework.array;

describe("framework/array", function () {
    "use strict";
    describe("randArrIndex", function () {
        it("should return a random key from the array", function () {
            var arr = ["ola", "amigo"],
                toTest = array.randArrIndex(arr);
                console.log(toTest);
            assert.isNumber(toTest);
            assert.isTrue(arr[toTest] !== undefined);
        });
        it("should return null if null was given", function () {
            assert.isNull(array.randArrIndex(null));
            assert.isNotNull(array.randArrIndex([0, 1, 2]));
        });
    });

    describe("randArrValue", function () {
        it("should return a random value from the array", function () {
            var arr = ["ola", "amigo"],
                toTest = array.randArrValue(arr);
            assert.include(arr, toTest);
        });
        it("should return null if null was given", function () {
            assert.isNull(array.randArrValue(null));
            assert.isNotNull(array.randArrValue([0, 1, 2]));
        });
    });

});
