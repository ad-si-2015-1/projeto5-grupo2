/*jslint node:true*/
var itensCrime = require("./src/entidades/itens-crime"),
    array = require("./src/framework/array"),
    jogada = require("./src/entidades/jogada");


var toExport = {
    entidades: {
        itensCrime: itensCrime,
        jogada: jogada
    },
    framework: {
        array: array
    }
};

module.exports = toExport;
