/*jslint node:true*/


function randArrIndex(arr) {
    "use strict";
    return (arr !== null) ? Math.floor(Math.random() * arr.length) : null;
}

function randArrValue(arr) {
    "use strict";
    return (arr !== null) ? arr[randArrIndex(arr)] : null;
}


module.exports = {
    "randArrIndex": randArrIndex,
    "randArrValue": randArrValue
};
