/*jslint node:true*/

var Arma = function (codigo, nome) {
    "use strict";
    this.codigo = codigo;
    this.nome = nome;
}


var armas = {
    REVOLVER : new Arma(1, "Revolver"),
    FACA : new Arma(2, "Faca"),
    ESCOPETA : new Arma(3, "Escopeta"),
    PA : new Arma(4, "Pá"),
    BOMBA : new Arma(5, "Bomba"),
    VENENO : new Arma(6, "Veneno"),
    TACO_BASEBOL : new Arma(7, "Bastão de Basebol"),
    ESTILINGUE : new Arma(8, "Estilingue / Atiradeira"),
    ADAGA : new Arma(9, "Adaga"),
    GARRAFA : new Arma(10, "Garrafa de vinho"),
    CORDA : new Arma(11, "Corda")
};

module.exports.Armas = Object.freeze(armas);
