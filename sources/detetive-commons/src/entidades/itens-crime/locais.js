/*jslint node:true*/

var Local = function (codigo, nome) {
    "use strict";
    this.codigo = codigo;
    this.nome = nome;
};


var locais = {
    SALA_ESTAR : new Local(1, "Sala de estar"),
    JARDIM : new Local(2, "Jardim da casa"),
    COZINHA : new Local(3, "Cozinha"),
    BIBLIOTECA : new Local(4, "Biblioteca"),
    QUARTO : new Local(5, "Quarto"),
    SALAO_FESTAS : new Local(6, "Salao de festas"),
    HALL : new Local(7, "Hall"),
    SALA_JANTAR : new Local(8, "Sala de jantar")
};

module.exports.Locais = Object.freeze(locais);
