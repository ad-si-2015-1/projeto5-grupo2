/*jslint node:true*/

var Suspeito = function (codigo, nome, descricao) {
    "use strict";
    this.codigo = codigo;
    this.nome = nome;
    this.descricao = descricao;
};

var suspeitos = {
    SARGENTO_BIGODE : new Suspeito(1, "Sargento Bigode",
        "Sargento do exercito da cidade é um antigo amigo do Sr. Pessoa que teve uma história de amor com a mulher de pessoa no passado"),
    FLORISTA_DONA_BRANCA : new Suspeito(2, "Florista Dona Branca",
        "Trabalha na floricultura da cidade e eh prima do Sr. Pessoa. Recentemente descobriu que faz parte da herança do Sr. Pessoa"),
    CHEFE_DE_COZINHA_TONY : new Suspeito(3, "Chefe de cozinha Tony",
        "Trabalha para o Sr. Pessoa a varios anos. Sempre foi um excelente cozinheiro, mas um homem completamente sistematico e obscuro"),
    MORDOMO_JAMES : new Suspeito(4, "Mordomo James",
        "O homem que conhece cada canto da mansao nos minimos detalhes. Pelo tempo que serve ao Sr. Pessoa era um conhecedor de todos os segredos da vítima."),
    DOUTORA_VIOLETA : new Suspeito(5, "Doutora Violeta",
        "Medica do Sr. Pessoa. Uma jovem muito bonita, talentosa e atraente. Recentemente havia se tornado muito intima ao Sr. Pessoa, ao passo que alguns desconfiam dessa intimidade"),
    DANCARIA_STA_ROSA : new Suspeito(6, "Dançarina Sra Rosa",
        "A Sta Rosa trabalha em uma casa de dança para adultos que era bastante frequentada pelo Sr. Pessoa. Sempre mostrou interesse especial pelo Sr. Pessoa"),
    GUARDA_SERGIO_SOTURNO : new Suspeito(7, "Guarda Sérgio Soturno",
        "Chefe da guarda da mansao de Sr. Pessoa. Ele tem uma paixão pela Sta Rosa e nunca se conformou da jovem dar tanta atenção ao Sr. Pessoa e não a ele."),
    ADVOGADO_SR_MARINHO : new Suspeito(8, "Advogado Sr. Marinho",
        "Advogado e conhecedor dos negócios do Sr. Pessoa. Pelo seu excelente trabalho tornou-se socio do Sr. Pessoa em varios investimentos e sempre foi o responsavel pela protecao dos bens do Sr. Pessoa")
};

module.exports.Suspeitos = Object.freeze(suspeitos);
