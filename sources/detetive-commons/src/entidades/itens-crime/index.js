/*jslint node:true*/


module.exports = {
    Armas: require("./armas").Armas,
    Locais: require("./locais").Locais,
    Suspeitos : require("./suspeitos").Suspeitos
};
