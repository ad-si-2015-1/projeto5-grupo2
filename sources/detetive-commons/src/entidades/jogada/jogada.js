/*jslint node:true */

function Jogada(tipo, itens) {
    "use strict";
    this.tipo = tipo;
    this.itens = itens;

    this.getTipo = function () {
        return this.tipo;
    };

    this.getItens = function () {
        return this.itens;
    };

    this.isEqualTo = function (outraJogada) {
        if (!outraJogada instanceof Jogada) {
            return false;
        }

        var essesItens = this.getItens(),
            aquelesItens = outraJogada.getItens();
        return essesItens.arma.codigo === aquelesItens.arma.codigo &&
            essesItens.suspeito.codigo === aquelesItens.suspeito.codigo &&
            essesItens.local.codigo === aquelesItens.local.codigo;
    };

    this.toString = function () {
        return {
            tipo: this.getTipo().getTipo(),
            arma: this.getItens().arma.nome,
            suspeito: this.getItens().suspeito.nome,
            local: this.getItens().local.nome
        };
    };
}

module.exports = Jogada;
