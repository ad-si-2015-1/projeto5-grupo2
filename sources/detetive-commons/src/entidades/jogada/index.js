/*jslint node:true*/
var Jogada = require("./jogada"),
    TiposJogada = require("./tipo-jogada");

module.exports = {
    TipoJogada : Object.freeze(TiposJogada),
    Jogada: Jogada
};
