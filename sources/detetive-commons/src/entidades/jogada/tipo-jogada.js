/*jslint node:true */

function TipoJogada(tipo) {
    "use strict";
    this.tipo = tipo;

    this.getTipo = function () {
        return this.tipo;
    };

    this.equals = function (other) {
        return other instanceof TipoJogada &&
            this.getTipo() === other.getTipo();
    };
}


var TiposJogada = {
    PALPITE: new TipoJogada("PALPITE"),
    ACUSACAO: new TipoJogada("ACUSACAO"),
    SOLUCAO: new TipoJogada("SOLUCAO")
};


TiposJogada.fromString = function (stringTipo) {
    "use strict";
    if (TiposJogada[stringTipo.toUpperCase()] !== undefined) {
        return TiposJogada[stringTipo.toUpperCase()];
    }
    return null;
};

module.exports = TiposJogada;
