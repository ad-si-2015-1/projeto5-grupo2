/*jslint browser:true, devel:true */
/*globals requirejs */

console.log("carregando aplicacao");
requirejs.config({
    //By default load any module IDs from js/lib
    baseUrl: '/js/',
    //except, if the module ID starts with "app",
    //load it from the js/app directory. paths
    //config is relative to the baseUrl, and
    //never includes a ".js" extension since
    //the paths config could be for a directory.
    paths: {
        "jquery": "vendor/jquery",
        "socket.io": "../socket.io"
    }
});

// Start the main app logic.
function startupHandler(Game) {
    "use strict";
    window.jogodetetive = new Game();
}
var dependencies = [
    "jogodetetive/index"
];

requirejs(dependencies, startupHandler);
