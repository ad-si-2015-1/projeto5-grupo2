/*jslint browser:true, devel:true , plusplus:true*/
/*globals define, requirejs */


var dependencies = [
    "jquery",
    "jogodetetive/domain"
];

define(dependencies, function ($, domain) {
    "use strict";

    function FrontEnd() {

        domain.setFrontEndHandler(this);

        var self = this,
            config = {
                palcoId: "jogo-detetive",
                itensCrimeContainer: $("<div>").attr("id", "itens-crime-container")
            },
            palcoJogo = $("#" + config.palcoId);

        palcoJogo.append(config.itensCrimeContainer);

        this.populateStage = function () {
            domain.loadGameItens(this.popularItensCrime);
            this.loadGameControls();
        };


        this.popularItensCrime = function (itensCrime) {

            function clickHandler(event) {
                console.log("Clicou em " + event.data.itemCrime.nome);
                domain.addItemSelecionado(event.data.label, event.data.itemCrime);

                var elemento = $(this);
                elemento.siblings().removeClass("selected-item");
                elemento.toggleClass("selected-item");

            }

            function popularItensCrimeTipo(label, itemCrimeEnum) {
                var todosItensTipoEl = $("<div>").attr("id", label + "-container-id"),
                    itensNames = Object.keys(itemCrimeEnum),
                    i,
                    itemEl,
                    enumVal;

		todosItensTipoEl.addClass("panel panel-info");

                for (i = 0; i < itensNames.length; i++) {
                    enumVal = itemCrimeEnum[itensNames[i]];
                    itemEl = $("<div>").attr({
                        "id" : label + enumVal.codigo,
                        "class" : "item-crime " + label
                    })
                        .html(enumVal.nome)
                        .click({"itemCrime": enumVal, "label": label}, clickHandler);

                    todosItensTipoEl.append(itemEl[0]);
                }
                config.itensCrimeContainer.append(todosItensTipoEl);
            }

            popularItensCrimeTipo("arma", itensCrime.Armas);
            popularItensCrimeTipo("suspeito", itensCrime.Suspeitos);
            popularItensCrimeTipo("local", itensCrime.Locais);
        };

        this.loadGameControls = function () {
            var palpiteButtonEl = $("<button>").attr("id", "lancar-palpite"),
                acusacaoButtonEl = $("<button>").attr("id", "lancar-acusacao"),
                controlesContainer = $("<div>").attr("id", "controle-jogada-container");

            palpiteButtonEl.text("Lançar palpite").addClass("btn btn-sucess");
            acusacaoButtonEl.text("Lançar acusação").addClass("btn btn-danger");

            function clickHandler(event) {
                var tipoJogada = event.data.tipo;
                if (domain.validarItensSelecionados()) {
                    console.log("Enviando jogada para o servidor...");
                    domain.enviarJogada(self.resultadoJogadaHandler);
                } else {
                    alert("Você deve selecionar um item de cada tipo para jogar.");
                }

            }
            palpiteButtonEl.click({tipo: "palpite"}, clickHandler);
            acusacaoButtonEl.click({tipo: "acusacao"}, clickHandler);
            controlesContainer.append([palpiteButtonEl, acusacaoButtonEl]);
            palcoJogo.append(controlesContainer);
        };

        function onGameover(mensagem) {
            alert("O jogo terminou! " + mensagem);
        }

        this.resultadoJogadaHandler = function (sucesso, gameover, dica, mensagem) {
            if (gameover) {
                onGameover(mensagem);
                return;
            }
            if (sucesso) {
                alert("Você venceu o jogo!");
            } else {
                alert("Você errou a escolha de " + dica.campo);
            }
        };


    }

    return new FrontEnd();
});
