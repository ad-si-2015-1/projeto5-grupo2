/*jslint browser:true, devel:true */
/*globals define, requirejs, io */


var dependencies = [
    "socket.io/socket.io.js"
];

define(dependencies, function (socketio) {
    "use strict";
    console.log("iniciando jogo");

    function GameDomain() {
        var socket = socketio(),
            frontendHandler,
            resultJogadaCalback,
            gameIsOver = false,
            itensSelecionados = {
                arma: null,
                suspeito: null,
                local: null
            };

        this.loadGameItens = function (callback) {
            socket.emit("init-game");
            socket.on("load-game-itens", callback);
        };

        this.addItemSelecionado = function (label, item) {
            itensSelecionados[label] = item;
        };

        this.getItensSelecionados = function () {
            return itensSelecionados;
        };

        this.enviarJogada = function (onResult) {
            if (gameIsOver) {
                this.onResultadoJogada(false, gameIsOver, null, "O jogo terminou");
            } else if (!this.validarItensSelecionados()) {
                alert("Voce precisa selecionar um de cada item para jogar");
            } else {
                socket.emit("jogada", this.getItensSelecionados());
                resultJogadaCalback = onResult;
            }
        };

        this.onResultadoJogada = function (sucesso, gameover, dica, mensagem) {
            gameIsOver = gameover;
            resultJogadaCalback(sucesso, gameover, dica, mensagem);
        };

        this.validarItensSelecionados = function () {
            var itens = this.getItensSelecionados();
            return itens.arma !== null &&
                itens.suspeito !== null &&
                itens.local !== null;
        };

        this.setFrontEndHandler = function (front) {
            frontendHandler = front;
        };

        socket.on("resultado-jogada", this.onResultadoJogada);
    }

    return new GameDomain();
});
