/*jslint browser:true, devel:true */
/*globals define, requirejs */


var dependencies = [
    "jogodetetive/frontend.mgr"
];

define(dependencies, function (front) {
    "use strict";
    console.log("iniciando jogo");

    function Game() {
        front.populateStage();
    }

    return Game;
});
