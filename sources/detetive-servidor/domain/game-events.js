/*jslint node:true, devel:true, todo: true */
var detetiveCommons = require("detetive-commons"),
    itensCrime = detetiveCommons.entidades.itensCrime,
    arrayUtils = detetiveCommons.framework.array,
    TipoJogada = detetiveCommons.entidades.jogada.TipoJogada,
    Jogada = detetiveCommons.entidades.jogada.Jogada;

function randProperty(obj) {
    "use strict";
    var arr = Object.keys(obj);
    return obj[arr[arrayUtils.randArrIndex(arr)]];
}

function criarSolucao() {
    "use strict";
    return new Jogada(TipoJogada.SOLUCAO, {
        arma: randProperty(itensCrime.Armas),
        local: randProperty(itensCrime.Locais),
        suspeito: randProperty(itensCrime.Suspeitos)
    });
}

var solucao = criarSolucao();

function calcularDica(jogada) {
    "use strict";
    var campos = [],
        itensPalpite = jogada.getItens(),
        itensSolucao = solucao.getItens();

    function pushIfTrue(shoudAdd, val) {
        return shoudAdd && campos.push(val);
    }

    pushIfTrue(itensPalpite.arma.codigo !== itensSolucao.arma.codigo, "arma");
    pushIfTrue(itensPalpite.suspeito.codigo !== itensSolucao.suspeito.codigo, "suspeito");
    pushIfTrue(itensPalpite.local.codigo !== itensSolucao.local.codigo, "local");

    return {
        campo: arrayUtils.randArrValue(campos)
    };
}

function jogadaCorreta(jogada) {
    "use strict";
    return jogada.isEqualTo(solucao);
}

function apply(socket) {
    "use strict";
    socket.on("init-game", function () {
        console.log("inicializando interface do jogo para o cliente X");
        socket.emit("load-game-itens", itensCrime);
    });
    socket.on("jogada", function (jogadaFrontend) {
        /** TODO disponibilizar classe de jogada para o browser */
        var dica,
            gameover,
            mensagem,
            tipoJogada = TipoJogada.fromString(jogadaFrontend.tipoJogada),
            jogada = new Jogada(tipoJogada, jogadaFrontend.itensSelecionados);


        console.log("recebida uma jogada!", jogada.toString());
        if (jogadaCorreta(jogada)) {
            socket.emit("resultado-jogada", true);
            socket.broadcast.emit("game-over", jogada.getItens(), socket.id);
        } else {
            gameover = tipoJogada.equals(TipoJogada.ACUSACAO);
            if (!gameover) {
                dica = calcularDica(jogada);
            }
            mensagem = gameover ? "O jogo terminou. Voce errou a acusacao" : "";
            socket.emit("resultado-jogada", false, gameover, dica, mensagem);
        }
    });
}

function getSolucao() {
    "use strict";
    return solucao;
}

module.exports = {
    apply: apply,
    getSolucao: getSolucao,
    TipoJogada: TipoJogada
};
