var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var gameevents = require("./domain/game-events");
var gamedata = require("./domain/game-data");

app.get('/', function(req, res){
    res.sendFile(__dirname + '/views/index.html');
});

app.get('/game', function(req, res){
    res.sendFile(__dirname + '/views/game.html');
});

app.get('/game/solucao', function(req, res){
    res.send(gameevents.getSolucao().toString());
});

app.get('/game/shared/js/jogodetetive.js', function(req, res){
    res.set("Content-Type", "application/javascript");
    res.sendFile(__dirname + "/public/js/jogodetetive.js");
});

app.use(express.static('public'));

http.listen(3000, function(){
  console.log('listening on *:3000');
});


io.on('connection', function(socket){
    gameevents.apply(socket);
});
