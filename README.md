Projeto 4 - Aplicações Distribuídas
===================================

Essa é a página Wiki do Projeto 5 da disciplina de Aplicações Distribuídas, ministrada pelo Prof. Me. Marcelo Akira Inozuka pela Universidade Federal de Goiás, primeiro semestre do ano letívo de 2015;

O Grupo 2 é formado pelos alunos:
- Ana Letícia Herculano
- Bruno Nogueira de Oliveira
- Daniel Melo
- Douglas Pose de Oliveira
- Guilherme Pinheiro de Souza e Silva

O objetivo do projeto é construir um Jogo Multiusuário que possa ser jogado de diferentes estações de trabalho; No projeto 5 o foco é WebSockets.

O grupo optou pelo desenvolvimento do jogo [Detetive](http://pt.wikipedia.org/wiki/Detetive_%28jogo%29).

Conheça nossa Wiki e saiba como colaborar com o projeto.

[Clique Aqui](https://gitlab.com/ad-si-2015-1/projeto4-grupo2/wikis/home)

Aprendizagem
------------

No projeto número 1 e 2 adotamos um processo de fluxo de trabalho mais corporativo, ou seja, havia um controle centralizado sobre o código e as ações dos colaboradores do projeto. Nos projetos 3, 4 e 5 adotamos uma postura mais OpenSource, ou seja, aconselhamos fortemente que você entre e contribua com o projeto (seguindo as regras definidas na Wiki, claro). Não precisa pedir benção para ninguém!


Estrutura de pastas
-------------------

O projeto tem algumas pastas que não devem ser violadas:
- cliente: pasta com fluxos do lado do jogador
- servidor: pasta com fluxos do lado do servidor

Entenda fácil
-------------

Ainda que seja um projeto OpenSource existem regras para colaborar e elas devem ser seguidas. Leia a Wiki para entender como as coisas funcionam por aqui. Aqui vou explicar de uma maneira fácil como colaborar;
1. Abra uma Issue informando o que você vai fazer: encontrou um bug que precisa ser resolvido? Quer adicionar uma funcionalidade? Quer dar uma sugestão ou inserir uma nova documentação? Antes de colaborar com o projeto abra uma Issue. Assim você evita que outra pessoa perca tempo fazendo algo que você já está fazendo.

2. Crie sua branch: o projeto não permite que você commit diretamente na branch master. Também não dá para commitar no PROD (aliás, nunca mexa no PROD). Clone o projeto e crie uma branch à partir da master. Use o nome que quiser, mas aconselhamos você nomear no seguinte padrão: "dev-issue-numIssue" (onde 'dev' é substituido por seu nome, 'issue' informa o tipo de atividade que você está resolvendo, 'numIssue' é o número da issue.  
Por exemplo: o Chico Bento encontrou um bug que ele registrou na atividade 45. Ele vai resolver então cria uma branch com o nome "chico-bug-45"

3. Comite suas contribuições informando o número da atividade: quando você faz um commit e passa na mensagem o número da tarefa antecedido de um cerquilha "#" o Gitlab automaticamente comenta na atividade informando o identificador do commit. Isso é bom para entender seu histórico pois ele é preenchido automaticamente quando você dá um push. Você pode informar o número da atividade em qualquer lugar da mensagem, mas uma boa prática é colocar no início.  
Seguindo o exemplo anterior o Chico Bento resolveu o bug da atividade 45 em um commit somente. Ele passou a mensagem do commit assim:  
[BUG #45] Resolvi o bug colocando um ponto e virgula no final da linha 10 da class DesenvolvedorNoob.java;

4. Faça merge request da sua contribuição: tem certeza de que resolveu uma atividade? Abriu Issue, commitou, testou e funcionou? Então agora é a hora de mandar sua contribuição. Crie um Merge Request da sua contribuição para a branch master. O lider do projeto, assim que avaliar sua contribuição vai aceitá-la e você vai ser um feliz ajudante da filosofia OpenSource.

Just do it
----------

Sim: uma frase da NIKE num projeto OpenSource. E por que? Porque acreditamos que um mundo melhor é feito de pessoas que se esforçam para fazer o melhor. Então se você quer um mundo melhor, faça você mesmo! Isso vale para os softwares!

Um grande abraço do amigo Urso!  
![ursinho_carinhoso](https://gitlab.com/ad-si-2015-1/projeto3-grupo2/uploads/d962bd673b44a1dbd377908ad8feb88c/ursinho_carinhoso.jpg)